
var username = "marin";
var convID = "41660dcd_18e0_4b08_9593_16899cc0f161";
var lastMessage = 0;



function loadMessages(shouldScroll) {
  console.log("Going to reload messages...");

  fetch("http://henri2h.fr:8080/Conversation/GetConversation", {
    method: "POST",
    headers: {
      "content-type": "application/json",
      "x-token": "token here",
      "x-username": username
    },
    body: JSON.stringify({
      "ConvID": convID
    })
  })
    .then(response => {
      //      console.log(response);
      return response.json();
    })
    .then((responseData) => { // responseData = undefined
      //    console.log(responseData);


      // On vérifie si le navigateur prend en charge
      // l'élément HTML template en vérifiant la présence
      // de l'attribut content pour l'élément template.
      if ("content" in document.createElement("template")) {

        // On prépare une ligne pour le tableau 
        var template = document.querySelector("#data_message");

        // On clone la ligne et on l'insère dans le tableau
        var tbody = document.querySelector("#messages");
        // we remove every element already in it
        while (tbody.firstChild) {
          tbody.removeChild(tbody.firstChild);
        }

        responseData["Messages"].forEach(element => {
          var clone = document.importNode(template.content, true);


          var mess_text = clone.querySelector(".data_message_text");
          mess_text.textContent = element["Content"];

          var mess_username = clone.querySelector(".data_message_username");
          mess_username.textContent = element["Username"];

          var mess_senddate = clone.querySelector(".data_message_senddate");
          mess_senddate.textContent = element["SendDate"];
          if (element["Username"] == username) {
            //clone.children[0].classList.add("has-background-primary");
            clone.querySelector(".box").classList.add("has-background-primary");
            clone.querySelector(".column").classList.add("is-offset-7");
          }


          // should scroll if new message
          if (element["MessageID"] > lastMessage) {
            lastMessage = element["MessageID"];
            if (shouldScroll == false) clone.querySelector(".box").classList.add("has-background-warning");
            shouldScroll = true;
          }
          tbody.appendChild(clone);


        });

        if (shouldScroll) {
          // got to the end of the page
          window.scrollTo(0, document.body.scrollHeight);
        }


      } else {
        // Une autre méthode pour ajouter les lignes
        // car l'élément HTML n'est pas pris en charge.
        console.log("Error ...");
      }

      return responseData;
    })
    .catch(err => {
      console.log(err);
    });
}

function sendMessage() {
  console.log("Going to send message");
  var messageComposer = document.querySelector("#message-composer");

  fetch("http://henri2h.fr:8080/Conversation/SendConversationMessage", {
    "method": "POST",
    "headers": {
      "content-type": "application/json",
      "x-token": "token here",
      "x-username": username
    },
    "body": JSON.stringify({
      "ConvID": convID,
      "Username": username,
      "Content": messageComposer.value,
      "DataType": "Text"
    })
  })
    .then(response => {
      console.log(response);
      // message send, we can reload
      loadMessages(true);
    })
    .catch(err => {
      console.log(err);
    });

  messageComposer.value = "";
}

function loadConversationsList() {
  var form = new FormData();

  fetch("http://henri2h.fr:8080/Conversation/ListConversations", {
    "method": "POST",
    "headers": {
      "content-type": "multipart/form-data; boundary=---011000010111000001101001",
      "x-token": "token here",
      "x-username": "henri2h"
    }
  })
    .then(response => {
      //console.log(response);
      return response.json();
    })
    .then((result) => {
      var dropConvID = document.querySelector("#drop-convID");
      // we remove every element already in it
      while (dropConvID.firstChild) {
        dropConvID.removeChild(dropConvID.firstChild);
      }

      result.forEach(element => {

        if (element["ConvID"] == convID) {
          document.querySelector(".data-convName").textContent = element["ConvName"];
        }
        //console.log(dropConvID);

        var opt = new Option(element["ConvName"], element["ConvID"]);
        dropConvID.appendChild(opt);
      });
    })
    .catch(err => {
      console.log(err);
    });
}


function loadUsersFromUI() {
  // load user

  var e = document.getElementById("drop-convID");
  convID = e.options[e.selectedIndex].value;
  console.log(convID);
  username = document.querySelector("#tb-username").value;
}


// should be called only once 
// no update on new message (color)
loadConversationsList();
//loadUsersFromUI();
loadMessages(true);


// send message on enter
// Get the input field
var input = document.querySelector("#message-composer");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function (event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    sendMessage();
  }
});


// regular call
function updateUI() {
  loadUsersFromUI();
  loadMessages(false);
}

// load new messages every 2.5 s
window.setInterval(updateUI, 1000);